import requests
import GHUrlBuilder

class GitHubAccess:
    builder = GHUrlBuilder.GHUrlBuilder()

    def GetContributors(self):
        req = requests.get(self.builder.GetUrlReposContributors())
        print(req.json())
