import sys
import getopt
import GitHubAccess

def GetArguments():
    options = []
    try:
        options, args = getopt.getopt(sys.argv[1:], "u:s:e:b:")
    except Exception as ex:
        print("Error: {}".format(ex))
    finally:
        return options


if __name__ == "__main__":
    argv = GetArguments()
    accessor = GitHubAccess.GitHubAccess()
    accessor.GetContributors()

 