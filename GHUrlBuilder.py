class GHUrlBuilder:
    api = "https://api.github.com"
    keyword_repos = "repos"
    keyword_users = "users"
    keyword_user = "user"
    repository = "golang/example"
    keyword_contributors = "contributors"
    keyword_commits = "commits"


    def UrlJoin(self, input_tuple):
        return "/".join(input_tuple)

    def GetUrlKeywordRepos(self):
        return self.UrlJoin((self.api, self.keyword_repos))

    def GetUrlRepository(self):
        return self.UrlJoin((self.GetUrlKeywordRepos(), self.repository))

    def GetUrlReposContributors(self):
        return self.UrlJoin((self.GetUrlRepository(), self.keyword_contributors))

    def GetUrlReposCommits(self):
        return self.UrlJoin((self.GetUrlKeywordRepos(), self.keyword_commits))
