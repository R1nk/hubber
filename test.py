import unittest
import GHUrlBuilder

class MyTestCase(unittest.TestCase):
    def test_should_return_repos_url(self):
        builder = GHUrlBuilder.GHUrlBuilder()
        self.assertEqual(builder.GetUrlRepository(), "https://api.github.com/repos/golang/example", "Wrong url request string")


if __name__ == '__main__':
    unittest.main()
